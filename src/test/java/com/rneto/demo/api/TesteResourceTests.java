package com.rneto.demo.api;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.rneto.demo.api.resource.TesteResource;

public class TesteResourceTests extends DemoApiApplicationTests {

	private MockMvc mockMvc;
	
	@Autowired
	private TesteResource testeResource;
	
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(testeResource).build();
	}
	
	@Test
	public void testGetTeste() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/teste")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testGetTesteById() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/teste/1")).andExpect(MockMvcResultMatchers.status().isOk());
	}
	
}
