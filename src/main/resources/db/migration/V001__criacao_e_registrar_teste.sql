CREATE TABLE teste (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(100) NOT NULL,
	valor DECIMAL(15,4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO teste set nome="Lazer", valor=1;
INSERT INTO teste set nome="Alimentação", valor=1.5;
INSERT INTO teste set nome="Supermercado", valor=2;
INSERT INTO teste set nome="Farmácia", valor=10.50;
INSERT INTO teste set nome="Outros", valor=51;
INSERT INTO teste set nome="Outros2";