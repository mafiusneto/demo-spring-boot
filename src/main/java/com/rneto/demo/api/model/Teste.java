package com.rneto.demo.api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import lombok.Data;

@Entity
@Table(name = "teste")
@Data
public class Teste {
	/*O uso da annotation @Data é referente a lib do Lombok, 
	 * onde a mesma deve ser importada no projeto e antes disso instalada na ide apenas executando o jar
	 * essa annotation simplifica automatiza a criação dos getters and setters e do hascode
	 * existe algumas annotatios proprias caso queira inibir a auto criação dos getters an setters
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@NotNull
	@Size(max = 100)
	private String nome;
	
	@NotNull
	private Double valor;
}
