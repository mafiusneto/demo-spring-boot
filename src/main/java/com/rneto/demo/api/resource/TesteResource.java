package com.rneto.demo.api.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rneto.demo.api.model.Teste;
import com.rneto.demo.api.repository.TesteRepository;

@RestController
@RequestMapping("/teste")
public class TesteResource {
	
	@Autowired
	private TesteRepository testeRep;
	
	@GetMapping
	public List<Teste> listar() {
		return testeRep.findAll();
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<Teste> buscarPeloCodigo(@PathVariable Long codigo) {
		Optional<Teste> teste = testeRep.findById(codigo);
		return teste != null ? ResponseEntity.ok(teste.get()) : ResponseEntity.notFound().build();
	}
	
	@PostMapping
	public ResponseEntity<Teste> criar(@Valid @RequestBody Teste teste, HttpServletResponse response) {
		Teste testeSalvo = testeRep.save(teste);
		
		//publisher.publishEvent(new RecursoCriadoEvent(this, response, categoriaSalva.getCodigo()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(testeSalvo);
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletarPeloCodigo(@PathVariable Long codigo) {
		testeRep.deleteById(codigo);
	}
}
