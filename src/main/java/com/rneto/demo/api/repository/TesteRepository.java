package com.rneto.demo.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rneto.demo.api.model.Teste;

public interface TesteRepository extends JpaRepository<Teste, Long>{

}
